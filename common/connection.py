import json
import logging
import os
import time

from common import register_connection

logger = logging.getLogger(__name__)


def get_config():
    con = register_connection.Connection(
        os.environ["REGISTER_ADDRESS"], os.environ["RRF_USERNAME"], os.environ["RRF_PASSWORD"]
    )
    while True:
        try:
            config = con.get_request("config?robot=" + os.environ["ROBOT_NAME"])["answer"]["value"]
            con.logout_user()
            break
        except Exception as e:
            logger.info("try to connect one more time " + str(e))
            time.sleep(2.0)
    return config


def play_voice(message):
    con = register_connection.Connection(
        os.environ["REGISTER_ADDRESS"], os.environ["RRF_USERNAME"], os.environ["RRF_PASSWORD"]
    )
    con.post_request("voice_generation/" + os.environ["ROBOT_NAME"], body={"text": message, "voice": "Joanna"})
    con.logout_user()


def set_config(config):
    con = register_connection.Connection(
        os.environ["REGISTER_ADDRESS"], os.environ["RRF_USERNAME"], os.environ["RRF_PASSWORD"]
    )
    while True:
        try:
            con.post_request("config?robot=" + os.environ["ROBOT_NAME"], body={"value": json.dumps(config)})
            con.logout_user()
            break
        except Exception as e:
            logger.info("try to connect one more time " + str(e))
            time.sleep(2.0)
    return config


def add_record(robot_name, filenames, params):
    con = register_connection.Connection(
        os.environ["REGISTER_ADDRESS"], os.environ["RRF_USERNAME"], os.environ["RRF_PASSWORD"]
    )
    logger.warning(str("logs" + "add_record/{0}/".format(robot_name)))
    con.post_request(
        "add_record/{0}".format(robot_name),
        module_name="logs",
        body={"filenames": json.dumps(filenames), "params": json.dumps(params)},
    )
    con.logout_user()


def update_config(config):
    con = register_connection.Connection(
        os.environ["REGISTER_ADDRESS"], os.environ["RRF_USERNAME"], os.environ["RRF_PASSWORD"]
    )
    while True:
        try:
            con.post_request("config?robot=" + os.environ["ROBOT_NAME"], body={"value": json.dumps(config)})
            con.logout_user()
            break
        except Exception as e:
            logger.info("try to connect one more time " + str(e))
            time.sleep(2.0)
    return config

def get_sessions():
    con = register_connection.Connection(
        os.environ["REGISTER_ADDRESS"], os.environ["RRF_USERNAME"], os.environ["RRF_PASSWORD"]
    )
    return con.get_request("sessions_data",module_name="sessions")['answer']

def get_tasks(author):
    con = register_connection.Connection(
        os.environ["REGISTER_ADDRESS"], os.environ["RRF_USERNAME"], os.environ["RRF_PASSWORD"]
    )

    tasks = con.get_request("tasks?author="+author)["answer"]

    con.logout_user()
    return tasks

def get_task(task_name_and_author):
    con = register_connection.Connection(
        os.environ["REGISTER_ADDRESS"], os.environ["RRF_USERNAME"], os.environ["RRF_PASSWORD"]
    )
    res_task = None
    ss = task_name_and_author.split(":")

    if len(ss) == 2:
        task_name = ss[0]
        author = ss[1]

        res_task = con.get_request("task?name="+task_name+"&author="+author)["answer"]
        res_task=res_task
        #for task in tasks:
        #    if task["name"] == task_name and task["author"] == author:
        #        res_task = task["task"]

    con.logout_user()

    return res_task


def put_task(task_name_and_author, task):
    con = register_connection.Connection(
        os.environ["REGISTER_ADDRESS"], os.environ["RRF_USERNAME"], os.environ["RRF_PASSWORD"]
    )

    ss = task_name_and_author.split(":")
    res = False

    if len(ss) == 2:
        task_name = ss[0]
        author = ss[1]

        res = con.put_request("task?name=" + task_name + "&" + "author=" + author, body=task)["answer"]

    con.logout_user()

    return res
