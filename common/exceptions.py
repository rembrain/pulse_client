class RequestConnectionError(Exception):
    def __init__(self, message):
        self.message = message


class AuthError(RequestConnectionError):
    pass


class LogoutError(RequestConnectionError):
    pass


class RequestError(RequestConnectionError):
    pass
