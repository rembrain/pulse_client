import time
import json
from rembrain_robot_framework import RobotProcess
import math
import threading
import numpy
import cv2
from scipy.spatial.transform import Rotation as R
from matplotlib.colors import LinearSegmentedColormap
import common.connection
from common import connection
from sklearn.decomposition import PCA
import os
import sys

class Viewer(RobotProcess):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        th=threading.Thread(target=self.ui_thread)
        th.daemon=True
        th.start()
        self.p2d=None
        self.depth=None
        self.state= None
        self.locker=threading.Lock()
        self.show_norm=False
        self.world_norm=None
        self.camera_matrix=None

        self.w=640
        self.h=480
        self.img=numpy.zeros((self.h,self.w,3),dtype=numpy.uint8)
        self.map = numpy.zeros((31*8, 31*8, 3), dtype=numpy.uint8)

        self.cfg = json.loads(connection.get_config())

        self.RT=numpy.array(self.cfg['calibration'])
        self.home_pos=self.cfg['park_joints']
        self.push_down=self.cfg['push_down']
        self.throw_position=numpy.array(self.cfg['throw_position'])

        self.cm = LinearSegmentedColormap.from_list("my_colormap", ["red", "green","blue"], N=256)
        self.pca=PCA()

        self.th_mincos=0.6

    def ui_thread(self):
        cv2.namedWindow('img')
        cv2.setMouseCallback("img", self.on_click)
        while True:
            key=cv2.waitKey(20)

            img_copy=self.img.copy()
            if self.show_norm:
                self.draw_norm(img_copy,self.camera_norm,self.camera_p3d)

            with self.locker:
                cv2.imshow('img',img_copy)
            cv2.imshow('map',self.map)

            if key==ord('q'):
                sys.exit(0)

            update_norm=False
            if key== ord('z'):
                self.angle1 = 0
                self.angle2 = 0
                update_norm=True

            if key == ord('a'):
                self.angle1 += 3
                update_norm = True
            if key == ord('d'):
                self.angle1 -= 3
                update_norm = True
            if key == ord('s'):
                self.angle2 += 3
                update_norm = True
            if key == ord('w'):
                self.angle2 -= 3
                update_norm = True

            if update_norm:
                with self.locker:
                    if self.calculate(False):
                        self.show_norm=True
                    else:
                        self.show_norm = False
                        self.world_norm=None

            if key==32:
                if not self.world_norm is None:
                    world_norm=-self.world_norm
                    p1=numpy.dot(self.RT,[self.camera_p3d[0],self.camera_p3d[1],self.camera_p3d[2],1])[0:3]

                    oy=-numpy.cross([1,0,0],world_norm)
                    r1=numpy.array([[1,0,0],oy,world_norm]).transpose()
                    p1 = p1 + numpy.dot(r1,numpy.array([0,0,self.push_down]))
                    p2=self.throw_position
                    r2=R.from_quat([-0.707, -0.707, 0, 0]).as_matrix()
                    command = {'op': 'pickup_putdown', 'source': 'operator', 'pos1': p1.tolist(),
                               'rot1': r1.tolist(), 'pos2': p2.tolist(), 'rot2': r2.tolist()}
                    print(command)
                    self.publish(json.dumps(command), 'viewer_to_robot')

            if key==ord('r'):
                self.publish(json.dumps({"op":"reset_state","source":"operator"}), 'viewer_to_robot')
                print('reset arm')

            if key==ord('h'):
                self.publish(json.dumps({"op": "setJ", "joints":self.home_pos,"source": "operator"}), 'viewer_to_robot')

    def on_click(self,event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONUP:
            p2d = (x, y)
            self.angle1 = 0
            self.angle2 = 0

            with self.locker:
                if not self.img is None and not self.depth is None and not self.camera_matrix is None:
                    if self.get_point(p2d) and self.calculate(True):
                        self.show_norm=True
                    else:
                        self.show_norm=False
                        self.world_norm = None

    def get_point(self,p2d):
        z = self.depth[p2d[1], p2d[0]] / 1000.0
        if z == 0:
            return False
        self.camera_p3d = numpy.array([(p2d[0] - self.camera_matrix['ppx']) / self.camera_matrix['fx'] * z,
                                       (p2d[1] - self.camera_matrix['ppy']) / self.camera_matrix['fy'] * z, z])
        return True

    def calculate(self,calculate_pca):
        #get point cloud
        p2d=(int(self.camera_p3d[0]/self.camera_p3d[2]*self.camera_matrix['fx']+self.camera_matrix['ppx']),
             int(self.camera_p3d[1] / self.camera_p3d[2] * self.camera_matrix['fy']+self.camera_matrix['ppy']))

        if calculate_pca:
            cloud = self.get_cloud(p2d)
            self.pca.fit(cloud)
            norm = self.pca.components_[-1]
            if norm[2] > 0:
                norm = -norm
            self.pca_norm=norm
        norm_rotated = numpy.dot(R.from_euler('XYZ', [self.angle2, self.angle1, 0], degrees=True).as_matrix(), self.pca_norm)
        self.camera_norm = norm_rotated
        self.world_norm = numpy.dot(self.RT[0:3,0:3], self.camera_norm)

        map=self.get_map(p2d)
        self.map = cv2.resize(map, (self.map.shape[1], self.map.shape[0]),interpolation=cv2.INTER_NEAREST)

        if self.world_norm[2] < self.th_mincos:
            return False
        else:
            return True

    def get_cloud(self, p2d):
        points = []
        for x in range(p2d[0] - 15, p2d[0] + 16):
            for y in range(p2d[1] - 15, p2d[1] + 16):
                if x >= 0 and y >= 0 and x < self.depth.shape[1] and y < self.depth.shape[0]:
                    if self.depth[y, x] != 0:
                        p = numpy.array(
                            [(x - self.camera_matrix['ppx']) / self.camera_matrix['fx'] * self.depth[y, x] / 1000.0,
                             (y - self.camera_matrix['ppy']) / self.camera_matrix['fy'] * self.depth[y, x] / 1000.0,
                             self.depth[y, x] / 1000.0])
                        points.append(p)
        return points

    def get_map(self,p2d):
        map = numpy.zeros((31, 31, 3), dtype=numpy.uint8)
        for x in range(p2d[0] - 15, p2d[0] + 16):
            for y in range(p2d[1] - 15, p2d[1] + 16):
                if x >= 0 and y >= 0 and x < self.depth.shape[1] and y < self.depth.shape[0]:
                    if self.depth[y, x] != 0:
                        p = numpy.array([(x - self.camera_matrix['ppx']) / self.camera_matrix['fx'] * self.depth[y, x] / 1000.0,
                                         (y - self.camera_matrix['ppy']) / self.camera_matrix['fy'] * self.depth[y, x] / 1000.0,
                                         self.depth[y, x] / 1000.0])

                        dist = numpy.dot(self.camera_p3d - p, self.camera_norm)
                        min = -0.05
                        max = 0.05
                        if dist < min:
                            dist = min
                        if dist > max:
                            dist = max
                        color = self.cm(int((dist - min) / (max - min) * 255.0))
                        yi = y - (p2d[1] - 15)
                        xi = x - (p2d[0] - 15)
                        map[yi, xi, 0] = int(color[2] * 255.0)
                        map[yi, xi, 1] = int(color[1] * 255.0)
                        map[yi, xi, 2] = int(color[0] * 255.0)
        return map

    def convert_to_camera(self,world_norm):
        R = self.RT[0:3, 0:3]
        R_inv = numpy.linalg.inv(R)
        camera_norm = numpy.dot(R_inv, world_norm)
        return camera_norm

    def draw_norm(self,frame, norm, pos):
        p1 = pos
        p2 = pos + norm * 0.1
        p1 = (int(p1[0] / p1[2] * self.camera_matrix['fx'] + self.camera_matrix['ppx']),
              int(p1[1] / p1[2] * self.camera_matrix['fy'] + self.camera_matrix['ppy']))
        p2 = (int(p2[0] / p2[2] * self.camera_matrix['fx'] + self.camera_matrix['ppx']),
              int(p2[1] / p2[2] * self.camera_matrix['fy'] + self.camera_matrix['ppy']))
        cv2.circle(frame, p1, 3, (0, 0, 255), thickness=-1)
        cv2.line(frame, p1, p2, (255, 0, 0), thickness=2)

    def run(self):
        self.log.info(f"{self.__class__.__name__} started, name: {self.name}.")

        while True:
            if not self.is_empty(consume_queue_name="video_to_viewer"):

                image,depth,camera_matrix = self.consume(queue_name="video_to_viewer")

                with self.locker:
                    self.img=image.copy()
                    self.depth=depth.copy()

                    o = json.loads(camera_matrix)
                    if isinstance(o,dict) and 'fx' in o:
                        self.camera_matrix=o

            time.sleep(0.005)

            if not self.is_empty('state'):
                self.state = json.loads(self.consume('state'))
