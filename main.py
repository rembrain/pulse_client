import os
from envyaml import EnvYAML
from rembrain_robot_framework import RobotDispatcher
from rembrain_robot_framework.processes import WsRobotProcess,VideoUnpacker,CommandTimer
from processes.viewer import Viewer

if __name__ == "__main__":
    process_map = {

        "command_timer": CommandTimer,
        "command_sender": WsRobotProcess,
        "video_receiver":WsRobotProcess,
        "video_unpacker":VideoUnpacker,
        "viewer": Viewer,
        "sensor_receiver":WsRobotProcess,

    }

    config = EnvYAML(os.path.join(os.path.dirname(__file__), "config", "processes_config.yaml"))
    processes = {p: {"process_class": process_map[p]} for p in config["processes"]}
    project_description = {"project": "brainless_robot", "subsystem": os.environ["RRF_USERNAME"], \
			"robot": os.environ["RRF_USERNAME"]}

    robot_dispatcher = RobotDispatcher(
                config, processes, project_description=project_description, in_cluster=False
            )

    robot_dispatcher.start_processes()
    robot_dispatcher.run()
